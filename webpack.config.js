var path = require("path");
const webpack = require("webpack");
const merge = require("webpack-merge");
const CleanWebpackPlugin = require('clean-webpack-plugin');
const NpmInstallPlugin = require('npm-install-webpack-plugin');

const codeGenConfig = require('./configs/buildInfo.gen');
const babelConfig = require('./configs/babel');



const StatsWriterPlugin = require("webpack-stats-plugin").StatsWriterPlugin;





module.exports = [
  
function(env) {

  const isDevelopment = env === 'development';
  console.log(`This is a ${ isDevelopment ? "development" : "production"} build` );

  const baseConfig  = {
      mode: "development",
      entry: "./app/app.js",
      // devtool: "source-map",
      output: {
        path: path.resolve(__dirname, "app/dist"),
        filename: "app.bundle.js",
        publicPath: "/dist/"
      },
      plugins: [
        // Everything else **first**
        // new CleanWebpackPlugin(['app/dist']),
        new webpack.DefinePlugin({
          ENV_IS_DEVELOPMENT: isDevelopment,
          ENV_IS: JSON.stringify( env),
        }),
        new webpack.SourceMapDevToolPlugin({
            filename: '[name].map',
            noSources: false
        })

        // Write out stats file to build directory.
        // new StatsWriterPlugin({
        //   filename: "stats.json" // Default
        // })
    ]
  }; // end of baseConfig

  if ( isDevelopment ) {

      const devServerConfig = {
          devServer: {
              // contentBase set to app folder, instead of working Dir.
              contentBase: path.resolve(__dirname, "app"),
              // directory where files are served
              publicPath: "/dist/",
              watchContentBase: false,
              host: "0.0.0.0",
              //  hotOnly: true,
              // overlay: true,
          },
          plugins: [
              // new NpmInstallPlugin(), //FIX-ME: crashes
              new webpack.NamedModulesPlugin(),
              new webpack.HotModuleReplacementPlugin(),
              // adding customized plugin
              // {
              //   apply(compiler) {
              //     // plugin registered here
              //     // compiler.hooks.done.tap.apply(function(params){
              //     //   console.log(require('util').inspect(compiler.options));
              //     // });
              //     compiler.plugin("done", function(params){
              //       console.log(require('util').inspect(compiler.options));
              //     });
              //   }
              // }
          ]
      }; // end of devServerConfig


      return merge(
          baseConfig,
          babelConfig,
          codeGenConfig,
          devServerConfig
      );
    
  }
  else {
      return merge(
          baseConfig,
          babelConfig
      );
  }


}]