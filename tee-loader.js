const loaderUtils = require('loader-utils');

module.exports = function (source) {
    const options = loaderUtils.getOptions(this);
    let label = '';
    if ( options )
        label = options.label;


    console.groupCollapsed(`[tee-loader-${label}] : ${this.resource}`);
    // console.log(source);
    console.groupEnd();
    return source;

}