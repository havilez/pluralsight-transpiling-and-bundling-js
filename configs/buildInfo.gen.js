
const path = require('path');

console.log('[codegen-loader]');
module.exports = {

    module: {
        rules: [
            {
                // test: /\.js$/,
                test: [ /\.gen.js$/ ],
                exclude: /(node_modules|bower_components)/,
                use: [
                    {loader: "codegen-loader"},
                ]
            }
        ]
    },

    resolveLoader: {
        alias: {
            'codegen-loader' : path.resolve( __dirname, '../buildInfo.gen.js')
        }
    }
};
