
  export class  Scoring {
    constructor() {
      this.score = 0;
    }

    newGame() {
      this.score = 0;
    }
    tableauCardTurnedUp() {
      this.score += 5;
    }
    dropped(source, destionation) {
      this.score += scoreForMoving(source, destionation) || 0;
    }

    wasteRecycled () {
      this.score = Math.max(this.score - 100, 0);
    };
  }
  
function scoreForMoving(source, destionation) {
  if (destionation.name === "TableauPile") {
    if (source.name === "FoundationPile") {
      return -15;
    }
    return 5;
  }
  if (destionation.name === "FoundationPile") {
    if (source.name === "TableauPile" || source.name === "WastePile") {
      return 10;
    }
  }
}
  

  console.log( ENV_IS );

  if ( ENV_IS_DEVELOPMENT ) {
    console.log('[scoring] evaluating'); 
  }
  

  if (module.hot) {

    console.log('[scoring] HMR code executing!!')
    module.hot.accept(console.log.bind(console) );

    const doc = angular.element(document);
    const injector = doc.injector();

    if ( injector ) {
      const actualService = injector.get("scoring");
      const newScoringService = new Scoring();

      // just replace functions
      Object.keys(actualService)
        .filter(key => actualService[key] === "function")
        .forEach(key => actualService[key] = newScoringService[key]);
      doc.find('html').scope().$apply();

      console.info('[scoring] Hot Swapped!!');
    }

  }


// Scoring is referecne to class defnition, scoring  is service instance name
angular.module("klondike.scoring", [])
  .service("scoring", [Scoring]);
