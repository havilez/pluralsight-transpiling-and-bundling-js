// import "babel-polyfill";
// import "@babel/polyfill";
import "./klondike/scoring";
import "./klondike/klondike";
import "./klondike/board";
import "./klondike/game";

angular.module("solitaire", ["klondike", "ngDraggable"]);
